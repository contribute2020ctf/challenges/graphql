# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
FLAG = ENV["FLAG"] || "[tanuki]THIS_IS_THE_GRAPHQL_2__DEFAULT_FLAG_83062"

user = User.create!(username: "user1", password: "84K4eWJF.F9HvP9wt9f7TcoqXC-JT6HT8Nv-*9zW", password_confirmation: "84K4eWJF.F9HvP9wt9f7TcoqXC-JT6HT8Nv-*9zW")
graphicle = user.projects.create!(name: 'Graphicle', description: 'The perfect use of GraphQL')
graphicle.issues.create!(title: "Write more Graphicle jokes", description: "Since graphicle is perfect, let's spend our time writing jokes!")
graphicle.issues.create!(title: "Graphicle launch party", description: "There's not better way to launch Graphicle than with a party!")
graphicle.issues.create!(title: "Graphicle IDOR vulnerability", description: "#{FLAG}", confidential: true)
graphicle.issues.create!(title: "Find Graphicle mascot", description: "I think such an awesome product needs an awesome mascot!")
Hint.create!(text: 'What is the id of the missing issue?')
Hint.create!(text: 'In which endpoint can you reference issues by ID?')
Hint.create!(text: 'Given the mutations available, what do you need to create the use the endpoint?')
