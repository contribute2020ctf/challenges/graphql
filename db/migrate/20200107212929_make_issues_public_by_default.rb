class MakeIssuesPublicByDefault < ActiveRecord::Migration[6.0]
  def change
    change_column_null :issues, :confidential, false
    change_column :issues, :confidential, :boolean, :default=>false
  end
end
