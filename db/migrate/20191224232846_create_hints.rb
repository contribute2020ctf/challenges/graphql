class CreateHints < ActiveRecord::Migration[6.0]
  def change
    create_table :hints do |t|
      t.string :text

      t.timestamps
    end
  end
end
