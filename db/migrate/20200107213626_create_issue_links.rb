class CreateIssueLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :issue_links do |t|
      t.integer "source_id", null: false
      t.integer "target_id", null: false
      t.index ["source_id", "target_id"], name: "index_issue_links_on_source_id_and_target_id", unique: true
      t.index ["source_id"], name: "index_issue_links_on_source_id"
      t.index ["target_id"], name: "index_issue_links_on_target_id"

      t.timestamps
    end
  end
end
