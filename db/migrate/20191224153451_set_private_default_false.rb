class SetPrivateDefaultFalse < ActiveRecord::Migration[6.0]
  def change
    change_column_null :projects, :private, false
    change_column :projects, :private, :boolean, :default => false
  end
end
