module Mutations
    class CreateUser < BaseMutation
        field :user, Types::UserType, null: false
        field :password, String, null: false
        field :msg, String, null: false


        def resolve()
            return if context[:current_user]
            
            username = create_username()
            password = create_password()

            user = User.create!(
                username: username,
                password: password
            )

            crypt = ActiveSupport::MessageEncryptor.new(Rails.application.credentials.secret_key_base.byteslice(0..31))
            token = crypt.encrypt_and_sign("user-id:#{ user.id }")
            context[:session][:token] = token

            { user: user, msg: "You have been signed in. Happy hunting."}
        end

        def create_username
            return "user" + rand(10000..99999).to_s
        end

        def create_password
            charset = Array('a'..'z') + Array('A'..'Z') + Array('0'..'9')
            Array.new(12) { charset.sample }.join
        end
    end
end
