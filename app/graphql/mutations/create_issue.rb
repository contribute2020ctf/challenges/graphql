module Mutations
    class CreateIssue < BaseMutation
        argument :project_id, ID, required: true
        argument :title, String, required: true
        argument :description, String, required: true
        argument :confidential, Boolean, required: false

        field :issue, Types::IssueType, null: false

        def resolve(project_id: nil, title: nil, description: nil, confidential: false)
            return unless context[:current_user]

            user = User.find_by id: context[:current_user]
            project = Project.find(project_id)

            return unless user.id == project.user_id

            issue = project.issues.create!(title: title, description: description, confidential: confidential)
            { issue: issue }
        end
    end
end
