module Mutations
    class LinkIssues < BaseMutation
        argument :source_project_id, ID, required: true
        argument :source_issue_id, ID, required: true
        argument :target_project_id, ID, required: true
        argument :target_issue_id, ID, required: true

        field :success, Boolean, null: false

        def resolve(source_project_id: nil, source_issue_id: nil, target_project_id: nil, target_issue_id: nil)
            return unless context[:current_user]

            # Validate user created project and source issue is in project
            user = User.find_by id: context[:current_user]
            source_project = Project.find(source_project_id)
            return unless user.id == source_project.user_id && source_project.issues.find(source_issue_id)

            # Validate target issue exists in the target project
            target_issue = Issue.find(target_issue_id)
            return unless target_issue.project_id == Integer(target_project_id)

            link = IssueLink.create!(source_id: source_issue_id, target_id: target_issue_id)

            { success: true }
        end
    end
end
