module Mutations
    class SignInUser < BaseMutation
        argument :username, String, required: true
        argument :password, String, required: true

        field :user, Types::UserType, null: false

        def resolve(username: nil, password: nil)
            return unless username
            return unless password

            user = User.find_by username: username

            return unless user
            return unless user.authenticate(password)

            crypt = ActiveSupport::MessageEncryptor.new(Rails.application.credentials.secret_key_base.byteslice(0..31))
            token = crypt.encrypt_and_sign("user-id:#{ user.id }")

            context[:session][:token] = token

            { user: user }
        end
    end
end
