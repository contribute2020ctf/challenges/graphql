module Mutations
    class CreateProject < BaseMutation
        argument :name, String, required: true
        argument :description, String, required: true

        field :project, Types::ProjectType, null: false

        def resolve(name: nil, description: nil)
            return unless context[:current_user]

            user = User.find_by id: context[:current_user]

            project = user.projects.create!(name: name, description: description, private: true)
            { project: project }
        end
    end
end
