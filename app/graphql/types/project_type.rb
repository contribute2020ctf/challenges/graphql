module Types
    class ProjectType < BaseObject
        field :id, ID, null: false
        field :name, String, null: false
        field :description, String, null: false
        field :private, Boolean, null: false
        field :owner, UserType, null: true, method: :user
        field :issues, [IssueType], null: true, resolver_method: :project_issues 
        
        def project_issues
            Issue.issues_for(context[:current_user], object)
        end
    end
end
