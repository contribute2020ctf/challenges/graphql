module Types
  class MutationType < Types::BaseObject
    field :create_user, mutation: Mutations::CreateUser
    field :create_project, mutation: Mutations::CreateProject
    field :create_issue, mutation: Mutations::CreateIssue
    field :link_issues, mutation: Mutations::LinkIssues
  end
end
