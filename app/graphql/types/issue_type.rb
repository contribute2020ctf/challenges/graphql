module Types
    class IssueType < BaseObject
        field :id, ID, null: false
        field :title, String, null: false
        field :description, String, null: false
        field :confidential, Boolean, null: false
        field :related, [IssueType], null: true, method: :related_issues
    end
end
