module Types
    class HintType < BaseObject
        field :id, ID, null: false
        field :text, String, null: false
    end
end
