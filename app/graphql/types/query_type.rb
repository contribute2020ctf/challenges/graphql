module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    # Project field - returns project by id
    field :project, ProjectType, null: true do
      argument :id, ID, required: true
    end

    # Projects field - returns all non-private projects
    field :projects, [ProjectType], null: true

    # Hint field - returns a hint by id
    field :hint, HintType, null: false do
      argument :id, ID, required: true
    end

    field :current_user, UserType, null: true

    def project(id: nil)
      project = Project.find(id)

      project if project.private == false || ( project.user_id == context[:current_user].id if context[:current_user] )
    end

    def projects
      Project.projects_for(context[:current_user])
    end

    def hint(id: nil)
      Hint.find(id)
    end

    def current_user
      return unless context[:current_user]
      User.find(context[:current_user].id)
    end
  end
end
