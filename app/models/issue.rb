class Issue < ApplicationRecord
  belongs_to :project

  def self.issues_for(user, project)
    if user && user.id == project.user_id 
      user.projects.find(project.id).issues
    else
      project.issues.where(confidential: false)
    end
  end

  def related_issues
    related_issues = Issue.select(['issues.*', 'issue_links.id AS issue_link_id',
               'issue_links.target_id as issue_link_source_id'])
      .joins("INNER JOIN issue_links ON
             (issue_links.source_id = issues.id AND issue_links.target_id = #{id})
             OR
             (issue_links.target_id = issues.id AND issue_links.source_id = #{id})")
      .reorder('issue_link_id')
  end
end
