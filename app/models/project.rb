class Project < ApplicationRecord
    belongs_to :user
    has_many :issues

    validates :name, presence: true
    validates :description, presence: true
    
    def self.projects_for(user)
        if user
            Project.where(private: false).or(Project.where(user_id: user.id))
        else
            Project.where(private: false)
        end
    end
end
