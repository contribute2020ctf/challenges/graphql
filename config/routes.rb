Rails.application.routes.draw do
  # Root goes to page with link to /graphiql
  root 'static_pages#home'

  # Always start graphiql, even in production
  mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  post "/graphql", to: "graphql#execute"
end
