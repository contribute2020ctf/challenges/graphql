# Stolen^WInspired by https://thoughtbot.com/blog/rails-on-docker
FROM ruby:2.6.5-alpine

RUN apk add --no-cache build-base libxml2-dev libxslt-dev nodejs yarn sqlite sqlite-dev tzdata

WORKDIR /app
COPY Gemfile* /app/

RUN gem install bundler -v 2.1.1 \
    && bundle config set without 'test' \
    && bundle install

COPY . .

ARG rails_master_key
ENV RAILS_MASTER_KEY=$rails_master_key

ENV RAILS_ENV=production
ENV RAILS_SERVE_STATIC_FILES=true

RUN bundle exec rake assets:precompile \
    && yarn install --check-files

EXPOSE 3000
CMD ["bin/run"]
