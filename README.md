# README

## Running in docker

`docker run -p 3000:3000 registry.gitlab.com/contribute2020ctf/challenges/graphql:<challenge-1|2>`

### To Log to STDOUT

Add `-e RAILS_LOG_TO_STDOUT=true` to command above.
